FROM maven:3-jdk-11-slim AS build
WORKDIR /app
COPY . .

ENV MAVEN_OPTS="-Dhttps.protocols=TLSv1.2 -Dorg.slf4j.simpleLogger.log.org.apache.maven.cli.transfer.Slf4jMavenTransferListener=WARN -Dorg.slf4j.simpleLogger.showDateTime=true -Djava.awt.headless=true"
ENV MAVEN_CLI_OPTS="--batch-mode --errors --fail-at-end --show-version"
RUN mvn ${MAVEN_CLI_OPTS} clean package -am -DskipTests

RUN find . -type f -name "*with-dependencies.jar" -print0 | xargs --null -I{} mv {} /app/target.jar


# Deployment Image
FROM openjdk:11-jre-slim  AS deploy
COPY --from=build /app/target.jar ./target.jar

ENTRYPOINT ["java","-jar","./target.jar"]
