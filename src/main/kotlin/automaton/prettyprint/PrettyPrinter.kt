/*
 * Copyright (c) 2017, MontiCore. All rights reserved.
 *
 * http://www.se-rwth.de/
 */
package automaton.prettyprint

import automaton._ast.ASTAutomaton
import automaton._ast.ASTState
import automaton._ast.ASTTransition
import automaton._visitor.AutomatonVisitor

/**
 * Pretty prints automatons. Use [.print] to start a pretty
 * print and get the result by using [.getResult].
 *
 * @author Robert Heim
 */
class PrettyPrinter : AutomatonVisitor {
    /**
     * Gets the printed result.
     *
     * @return the result of the pretty print.
     */
    var result = ""
        private set

    private var indention = 0

    private var indent = ""

    /**
     * Prints the automaton
     *
     * @param automaton
     */
    fun print(automaton: ASTAutomaton) {
        handle(automaton)
    }

    override fun visit(node: ASTAutomaton) {
        println("automaton ${node.name} {")
        indent()
    }

    override fun endVisit(node: ASTAutomaton) {
        unindent()
        println("}")
    }

    override fun traverse(node: ASTAutomaton) {
        // guarantee ordering: states before transitions
        node.stateList.forEach { s -> s.accept(realThis) }
        node.transitionList.forEach { t -> t.accept(realThis) }
    }

    override fun visit(node: ASTState) {
        print("state ${node.name}")
        if (node.isInitial) {
            print(" <<initial>>")
        }
        if (node.isFinal) {
            print(" <<final>>")
        }
        println(";")
    }

    override fun visit(node: ASTTransition) {
        print(node.from)
        print(" - ${node.input} > ")
        print(node.to)
        println(";")
    }

    private fun print(s: String) {
        result += indent + s
        indent = ""
    }

    private fun println(s: String) {
        result += "$indent$s\n"
        indent = ""
        calcIndention()
    }

    private fun calcIndention() {
        indent = ""
        for (i in 0 until indention) {
            indent += "  "
        }
    }

    private fun indent() {
        indention++
        calcIndention()
    }

    private fun unindent() {
        indention--
        calcIndention()
    }
}
