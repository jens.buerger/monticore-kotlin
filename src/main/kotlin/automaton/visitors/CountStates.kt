/*
 * Copyright (c) 2017, MontiCore. All rights reserved.
 *
 * http://www.se-rwth.de/
 */
package automaton.visitors

import automaton._ast.ASTState
import automaton._visitor.AutomatonVisitor

/**
 * Counts the states of an automaton.
 *
 * @author Robert Heim
 */
class CountStates : AutomatonVisitor {
    var count = 0
        private set

    override fun visit(node: ASTState) {
        count++
    }
}
