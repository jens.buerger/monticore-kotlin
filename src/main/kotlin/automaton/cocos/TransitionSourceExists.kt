/*
 * Copyright (c) 2017, MontiCore. All rights reserved.
 *
 * http://www.se-rwth.de/
 */
package automaton.cocos

import automaton._ast.ASTTransition
import automaton._cocos.AutomatonASTTransitionCoCo
import automaton._symboltable.StateSymbol
import com.google.common.base.Preconditions.checkArgument
import de.se_rwth.commons.logging.Log
import java.util.*

class TransitionSourceExists : AutomatonASTTransitionCoCo {


    override fun check(node: ASTTransition) {
        checkArgument(node.isPresentEnclosingScope)

        val enclosingScope = node.enclosingScope
        val sourceState: Optional<StateSymbol> = enclosingScope.resolve(node.from, StateSymbol.KIND)

        if (!sourceState.isPresent) {
            // Issue error...
            Log.error("0xAUT03 The source state of the transition does not exist.",
                    node._SourcePositionStart)
        }
    }
}
