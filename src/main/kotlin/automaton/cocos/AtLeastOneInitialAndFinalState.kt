/*
 * Copyright (c) 2017, MontiCore. All rights reserved.
 *
 * http://www.se-rwth.de/
 */
package automaton.cocos

import automaton._ast.ASTAutomaton
import automaton._cocos.AutomatonASTAutomatonCoCo
import de.se_rwth.commons.logging.Log

class AtLeastOneInitialAndFinalState : AutomatonASTAutomatonCoCo {

    override fun check(automaton: ASTAutomaton) {
        var initialState = false
        var finalState = false

        for (state in automaton.stateList) {
            if (state.isInitial) {
                initialState = true
            }
            if (state.isFinal) {
                finalState = true
            }
        }

        if (!initialState || !finalState) {
            // Issue error...
            Log.error("0xA0114 An automaton must have at least one initial and one final state.",
                    automaton._SourcePositionStart)
        }
    }

}
