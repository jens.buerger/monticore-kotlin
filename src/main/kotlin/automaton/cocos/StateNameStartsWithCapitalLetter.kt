/*
 * Copyright (c) 2017, MontiCore. All rights reserved.
 *
 * http://www.se-rwth.de/
 */
package automaton.cocos

import automaton._ast.ASTState
import automaton._cocos.AutomatonASTStateCoCo
import de.se_rwth.commons.logging.Log

class StateNameStartsWithCapitalLetter : AutomatonASTStateCoCo {


    override fun check(state: ASTState) {
        val stateName = state.name
        val startsWithUpperCase = Character.isUpperCase(stateName[0])

        if (!startsWithUpperCase) {
            // Issue warning...
            Log.warn(
                    String.format("0xAUT02 State name '%s' should start with a capital letter.", stateName),
                    state._SourcePositionStart)
        }
    }

}
