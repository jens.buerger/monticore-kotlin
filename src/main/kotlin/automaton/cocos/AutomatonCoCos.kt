/*
 * Copyright (c) 2017, MontiCore. All rights reserved.
 *
 * http://www.se-rwth.de/
 */
package automaton.cocos

import automaton._cocos.AutomatonCoCoChecker

class AutomatonCoCos {

    // TODO PN uncomment
    // checker.addCoCo(new TransitionSourceExists());
    val checkerForAllCoCos: AutomatonCoCoChecker
        get() {
            val checker = AutomatonCoCoChecker()
            checker.addCoCo(AtLeastOneInitialAndFinalState())
            checker.addCoCo(StateNameStartsWithCapitalLetter())

            return checker
        }
}
