package automaton

/*
 * Copyright (c) 2017, MontiCore. All rights reserved.
 *
 * http://www.se-rwth.de/
 */

import automaton._ast.ASTAutomaton
import automaton._cocos.AutomatonCoCoChecker
import automaton._parser.AutomatonParser
import automaton._symboltable.AutomatonLanguage
import automaton._symboltable.StateSymbol
import automaton.cocos.AtLeastOneInitialAndFinalState
import automaton.cocos.AutomatonCoCos
import automaton.cocos.StateNameStartsWithCapitalLetter
import automaton.cocos.TransitionSourceExists
import automaton.prettyprint.PrettyPrinter
import automaton.visitors.CountStates
import de.monticore.io.paths.ModelPath
import de.monticore.symboltable.GlobalScope
import de.monticore.symboltable.ResolvingConfiguration
import de.monticore.symboltable.Scope
import de.monticore.symboltable.Symbol
import de.se_rwth.commons.logging.Log
import org.antlr.v4.runtime.RecognitionException
import java.io.IOException

/**
 * Main class for the Automaton DSL tool.
 *
 * @author (last commit) $Author$
 */
object AutomatonTool {

    /**
     * Use the single argument for specifying the single input automaton file.
     *
     * @param args
     */
    @JvmStatic
    fun main(args: Array<String>) {
        if (args.size != 1) {
            Log.error("Please specify only one single path to the input model.")
            return
        }
        val model = args[0]

        // setup the language infrastructure
        val lang = AutomatonLanguage()

        // parse the model and create the AST representation
        val ast = parse(model)
        Log.info("$model parsed successfully!", AutomatonTool::class.java.name)

        // setup the symbol table
        val modelTopScope = createSymbolTable(lang, ast)
        // can be used for resolving things in the model
        val aSymbol = modelTopScope.resolve<Symbol>("Ping", StateSymbol.KIND)
        if (aSymbol.isPresent) {
            Log.info("Resolved state symbol \"Ping\"; FQN = " + aSymbol.get().toString(),
                    AutomatonTool::class.java.name)
        }

        // execute default context conditions
        runDefaultCoCos(ast)

        // execute a custom set of context conditions
        val customCoCos = AutomatonCoCoChecker()
        customCoCos.addCoCo(StateNameStartsWithCapitalLetter())
        customCoCos.checkAll(ast!!)

        // analyze the model with a visitor
        val cs = CountStates()
        cs.handle(ast)
        Log.info("The model contains " + cs.count + " states.", AutomatonTool::class.java.name)

        // execute a pretty printer
        val pp = PrettyPrinter()
        pp.handle(ast)
        Log.info("Pretty printing the parsed automaton into console:", AutomatonTool::class.java.name)
        println(pp.result)
    }

    /**
     * Parse the model contained in the specified file.
     *
     * @param model - file to parse
     * @return
     */
    private fun parse(model: String): ASTAutomaton? {
        try {
            val parser = AutomatonParser()
            val optAutomaton = parser.parse(model)

            if (!parser.hasErrors() && optAutomaton.isPresent) {
                return optAutomaton.get()
            }
            Log.error("Model could not be parsed.")
        } catch (e: RecognitionException) {
            Log.error("Failed to parse $model", e)
        } catch (e: IOException) {
            Log.error("Failed to parse $model", e)
        }

        return null
    }

    /**
     * Create the symbol table from the parsed AST.
     *
     * @param lang
     * @param ast
     * @return
     */
    private fun createSymbolTable(lang: AutomatonLanguage, ast: ASTAutomaton?): Scope {
        val resolverConfiguration = ResolvingConfiguration()
        resolverConfiguration.addDefaultFilters(lang.resolvingFilters)

        val globalScope = GlobalScope(ModelPath(), lang, resolverConfiguration)

        val symbolTable = lang.getSymbolTableCreator(
                resolverConfiguration, globalScope)
        return symbolTable.get().createFromAST(ast)
    }

    /**
     * Run the default context conditions [AtLeastOneInitialAndFinalState],
     * [TransitionSourceExists], and
     * [StateNameStartsWithCapitalLetter].
     *
     * @param ast
     */
    private fun runDefaultCoCos(ast: ASTAutomaton?) {
        AutomatonCoCos().checkerForAllCoCos.checkAll(ast!!)
    }

}
