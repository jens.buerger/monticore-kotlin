/*
 * Copyright (c) 2017, MontiCore. All rights reserved.
 *
 * http://www.se-rwth.de/
 */
package automaton.symboltable


import automaton._symboltable.AutomatonLanguage
import automaton._symboltable.AutomatonSymbol
import de.monticore.io.paths.ModelPath
import de.monticore.symboltable.GlobalScope
import de.monticore.symboltable.ResolvingConfiguration
import de.monticore.symboltable.Scope
import io.kotlintest.matchers.types.shouldBeSameInstanceAs
import io.kotlintest.matchers.types.shouldNotBeNull
import io.kotlintest.shouldBe
import org.junit.jupiter.api.Assertions.assertSame
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.nio.file.Paths


class AutomatonSymbolTableCreatorTest {

    private var globalScope: Scope? = null

    @BeforeEach
    fun setup() {
        val automatonLanguage = AutomatonLanguage()

        val resolverConfiguration = ResolvingConfiguration()
        resolverConfiguration.addDefaultFilters(automatonLanguage.resolvingFilters)

        val modelPath = ModelPath(Paths.get("src/test/resources/automaton/symboltable"))

        globalScope = GlobalScope(modelPath, automatonLanguage, resolverConfiguration)
    }

    @Test
    fun testAutomatonSymbolTableCreation() {
        val automatonSymbol: AutomatonSymbol = globalScope!!.resolve<AutomatonSymbol>("PingPong", AutomatonSymbol.KIND).orElse(null)

        automatonSymbol.shouldNotBeNull()
        automatonSymbol.name shouldBe "PingPong"

        // assertEquals("PingPong", automatonSymbol.fullName)
        automatonSymbol.states.size shouldBe 3
        automatonSymbol.shouldBeSameInstanceAs(automatonSymbol.astNode.get().symbol)

        assertSame(automatonSymbol.enclosingScope, automatonSymbol.astNode.get()
                .enclosingScope)

        val noGameState = automatonSymbol.getState("NoGame").orElse(null)
        noGameState.shouldNotBeNull()
        noGameState.name shouldBe "NoGame"
        noGameState.astNode.get().symbol shouldBe noGameState
        noGameState.astNode.get().enclosingScope shouldBe noGameState.enclosingScope

        val pingState = automatonSymbol.getState("Ping").orElse(null)
        pingState.shouldNotBeNull()
        pingState.name shouldBe "Ping"

        val pongState = automatonSymbol.getState("Pong").orElse(null)
        pongState.shouldNotBeNull()
        pongState.name shouldBe "Pong"

    }

}
