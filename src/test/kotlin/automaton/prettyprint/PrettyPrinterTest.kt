/*
 * Copyright (c) 2017, MontiCore. All rights reserved.
 *
 * http://www.se-rwth.de/
 */
package automaton.prettyprint


import automaton.lang.AbstractTest
import io.kotlintest.shouldBe
import org.junit.jupiter.api.Test
import java.io.IOException

/**
 * Test for [PrettyPrinter].
 *
 * @author Robert Heim
 */
class PrettyPrinterTest : AbstractTest() {

    @Test
    @Throws(IOException::class)
    fun test() {
        val automaton = parseModel("src/test/resources/automaton/prettyprinter/valid/A.aut")
        val pp = PrettyPrinter()
        pp.handle(automaton)

        val actual = pp.result
        val expected = "automaton A {\n  state S <<initial>> <<final>>;\n}\n"
        expected shouldBe actual
    }

}
