/*
 * Copyright (c) 2017, MontiCore. All rights reserved.
 *
 * http://www.se-rwth.de/
 */
package automaton.parser


import automaton._parser.AutomatonParser
import automaton.lang.AbstractTest
import io.kotlintest.matchers.types.shouldNotBeNull
import io.kotlintest.shouldBe
import org.antlr.v4.runtime.RecognitionException
import org.junit.jupiter.api.Test
import java.io.IOException
import java.io.StringReader


class AutomatonParserTest : AbstractTest() {

    @Test
    fun testPingPong() {
        val a = parseModel("src/test/resources/automaton/parser/PingPong.aut")
        a.shouldNotBeNull()
    }

    @Test
    @Throws(RecognitionException::class, IOException::class)
    fun testState() {
        val parser = AutomatonParser()
        val state = parser.parseState(
                StringReader("state Ping;"))
        parser.hasErrors() shouldBe false
        state.isPresent shouldBe true
    }
}
