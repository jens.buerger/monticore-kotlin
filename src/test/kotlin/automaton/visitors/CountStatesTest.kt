/*
 * Copyright (c) 2017, MontiCore. All rights reserved.
 *
 * http://www.se-rwth.de/
 */
package automaton.visitors

import automaton.lang.AbstractTest
import io.kotlintest.shouldBe
import org.junit.jupiter.api.Test

/**
 * Test for [CountStates].
 *
 * @author Robert Heim
 */
class CountStatesTest : AbstractTest() {

    @Test
    fun test() {
        val automaton = parseModel("src/test/resources/automaton/visitors/valid/A.aut")
        val cs = CountStates()
        cs.handle(automaton)
        cs.count shouldBe 3
    }
}
