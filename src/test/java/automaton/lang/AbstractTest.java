/*
 * Copyright (c) 2017, MontiCore. All rights reserved.
 *
 * http://www.se-rwth.de/
 */
package automaton.lang;

import automaton._ast.ASTAutomaton;
import automaton._parser.AutomatonParser;
import org.antlr.v4.runtime.RecognitionException;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Provides some helpers for tests.
 *
 * @author Robert Heim
 */
abstract public class AbstractTest {

    /**
     * Parses a model and ensures that the root node is present.
     *
     * @param modelFile the full file name of the model.
     * @return the root of the parsed model.
     */
    protected ASTAutomaton parseModel(String modelFile) {
        Path model = Paths.get(modelFile);
        AutomatonParser parser = new AutomatonParser();
        Optional<ASTAutomaton> optAutomaton;
        try {
            optAutomaton = parser.parse(model.toString());
            assertFalse(parser.hasErrors());
            assertTrue(optAutomaton.isPresent());
            return optAutomaton.get();
        } catch (RecognitionException | IOException e) {
            e.printStackTrace();
            fail("There was an exception when parsing the model " + modelFile + ": "
                    + e.getMessage());
        }
        return null;
    }
}
