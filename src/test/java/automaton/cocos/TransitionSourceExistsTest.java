/*
 * Copyright (c) 2017, MontiCore. All rights reserved.
 *
 * http://www.se-rwth.de/
 */
package automaton.cocos;

import automaton._ast.ASTAutomaton;
import automaton._ast.ASTTransition;
import automaton._cocos.AutomatonCoCoChecker;
import automaton._symboltable.AutomatonLanguage;
import automaton._symboltable.AutomatonSymbol;
import automaton._symboltable.AutomatonSymbolTableCreator;
import automaton.lang.AbstractTest;
import de.monticore.ModelingLanguage;
import de.monticore.cocos.helper.Assert;
import de.monticore.io.paths.ModelPath;
import de.monticore.symboltable.GlobalScope;
import de.monticore.symboltable.ResolvingConfiguration;
import de.monticore.symboltable.Scope;
import de.se_rwth.commons.SourcePosition;
import de.se_rwth.commons.logging.Finding;
import de.se_rwth.commons.logging.Log;
import org.antlr.v4.runtime.RecognitionException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.file.Paths;
import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertTrue;

class TransitionSourceExistsTest extends AbstractTest {

    @BeforeAll
    static void init() {
        Log.enableFailQuick(false);
    }

    @BeforeEach
    void setUp() throws RecognitionException {
        Log.getFindings().clear();
    }

    @Test
    void testValid() {
        ModelPath modelPath = new ModelPath(Paths.get("src/test/resources/automaton/cocos/valid"));
        ModelingLanguage language = new AutomatonLanguage();
        ResolvingConfiguration resolverConfiguration = new ResolvingConfiguration();
        resolverConfiguration.addDefaultFilters(language.getResolvingFilters());
        Scope globalScope = new GlobalScope(modelPath, language, resolverConfiguration);

        Optional<AutomatonSymbol> automatonSymbol = globalScope.resolve("A", AutomatonSymbol.KIND);
        assertTrue(automatonSymbol.isPresent());
        //noinspection OptionalGetWithoutIsPresent
        ASTAutomaton automaton = (ASTAutomaton) automatonSymbol.get().getAstNode().get();

        AutomatonCoCoChecker checker = new AutomatonCoCos().getCheckerForAllCoCos();
        checker.checkAll(automaton);

        assertTrue(Log.getFindings().isEmpty());
    }

    @Test
    void testNotExistingTransitionSource() {
        ModelPath modelPath = new ModelPath(Paths.get("src/test/resources/automaton/cocos/invalid"));
        AutomatonLanguage language = new AutomatonLanguage();
        ResolvingConfiguration resolverConfiguration = new ResolvingConfiguration();
        resolverConfiguration.addDefaultFilters(language.getResolvingFilters());
        GlobalScope globalScope = new GlobalScope(modelPath, language, resolverConfiguration);

        ASTAutomaton ast = parseModel("src/test/resources/automaton/cocos/invalid/NotExistingTransitionSource.aut");

        Optional<AutomatonSymbolTableCreator> stc = language.getSymbolTableCreator(
                resolverConfiguration, globalScope);
        //noinspection OptionalGetWithoutIsPresent
        stc.get().createFromAST(ast);

        ASTTransition transition = ast.getTransitionList().get(0);

        TransitionSourceExists coco = new TransitionSourceExists();
        coco.check(transition);

        Collection<Finding> expectedErrors = Collections.singletonList(
                Finding.error("0xAUT03 The source state of the transition does not exist.",
                        new SourcePosition(23, 2))
        );

        Assert.assertErrors(expectedErrors, Log.getFindings());
    }

}
