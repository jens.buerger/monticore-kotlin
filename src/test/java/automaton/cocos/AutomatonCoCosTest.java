/*
 * Copyright (c) 2017, MontiCore. All rights reserved.
 *
 * http://www.se-rwth.de/
 */
package automaton.cocos;

import automaton._ast.ASTAutomaton;
import automaton._cocos.AutomatonCoCoChecker;
import automaton.lang.AbstractTest;
import de.monticore.cocos.helper.Assert;
import de.se_rwth.commons.SourcePosition;
import de.se_rwth.commons.logging.Finding;
import de.se_rwth.commons.logging.Log;
import org.antlr.v4.runtime.RecognitionException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Collections;

class AutomatonCoCosTest extends AbstractTest {

    @BeforeAll
    static void init() {
        Log.enableFailQuick(false);
    }

    @BeforeEach
    void setUp() throws RecognitionException {
        Log.getFindings().clear();
    }

    @Test
    void testWithChecker() {
        ASTAutomaton ast =
                parseModel("src/test/resources/automaton/cocos/invalid/StateDoesNotStartWithCapitalLetter.aut");

        AutomatonCoCoChecker checker = new AutomatonCoCoChecker();
        checker.addCoCo(new AtLeastOneInitialAndFinalState());
        checker.addCoCo(new StateNameStartsWithCapitalLetter());

        checker.checkAll(ast);

        Collection<Finding> expectedErrors = Collections.singletonList(
                Finding.warning("0xAUT02 State name 'notCapital' should start with a capital letter."
                        , new SourcePosition(22, 2))
        );
        Assert.assertErrors(expectedErrors, Log.getFindings());
    }

    @Test
    void testStateDoesNotStartWithCapitalLetter() {
        ASTAutomaton automaton = parseModel("src/test/resources/automaton/cocos/invalid/StateDoesNotStartWithCapitalLetter.aut");

        AutomatonCoCoChecker checker = new AutomatonCoCos().getCheckerForAllCoCos();
        checker.checkAll(automaton);

        Collection<Finding> expectedErrors = Collections.singletonList(
                Finding.warning("0xAUT02 State name 'notCapital' should start with a capital letter."
                        , new SourcePosition(22, 2))
        );

        Assert.assertErrors(expectedErrors, Log.getFindings());
    }

}
