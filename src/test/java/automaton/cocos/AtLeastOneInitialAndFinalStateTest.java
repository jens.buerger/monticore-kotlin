/*
 * Copyright (c) 2017, MontiCore. All rights reserved.
 *
 * http://www.se-rwth.de/
 */
package automaton.cocos;

import automaton._ast.ASTAutomaton;
import automaton.lang.AbstractTest;
import de.monticore.cocos.helper.Assert;
import de.se_rwth.commons.SourcePosition;
import de.se_rwth.commons.logging.Finding;
import de.se_rwth.commons.logging.Log;
import org.antlr.v4.runtime.RecognitionException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertTrue;


class AtLeastOneInitialAndFinalStateTest extends AbstractTest {

    @BeforeAll
    static void init() {
        Log.enableFailQuick(false);
    }

    @BeforeEach
    void setUp() throws RecognitionException {
        Log.getFindings().clear();
    }

    @Test
    void testValid() {
        ASTAutomaton automaton = parseModel("src/test/resources/automaton/cocos/valid/A.aut");

        AtLeastOneInitialAndFinalState coco = new AtLeastOneInitialAndFinalState();
        coco.check(automaton);

        assertTrue(Log.getFindings().isEmpty());
    }

    @Test
    void testMissingInitialState() {
        ASTAutomaton automaton = parseModel("src/test/resources/automaton/cocos/invalid/MissingInitialState.aut");

        AtLeastOneInitialAndFinalState coco = new AtLeastOneInitialAndFinalState();
        coco.check(automaton);

        Collection<Finding> expectedErrors = Collections.singletonList(
                Finding.error("0xA0114 An automaton must have at least one initial and one final state.",
                        new SourcePosition(20, 0))
        );

        Assert.assertErrors(expectedErrors, Log.getFindings());
    }

}
